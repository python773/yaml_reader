import sys

sys.path.insert(0, ".")
from pathlib import Path

from yaml_reader import YamlConfigReader


def test_get_config_correct_input() -> None:
    reader = YamlConfigReader(Path(r"_sample_data\config_example.yaml"))
    assert isinstance(reader.yaml_config_path, Path)


def test_get_config_inccorrect_input() -> None:
    """Class should repair incorrect input into Path object"""
    reader = YamlConfigReader(r"_sample_data\config_example.yaml")
    assert isinstance(reader.yaml_config_path, Path)


def test_get_config_correct_path() -> None:
    reader = YamlConfigReader(Path(r"_sample_data\config_example.yaml"))
    config = reader.get_config()
    assert bool(config) == True


def test_get_config_incorrect_path() -> None:
    reader = YamlConfigReader(Path(r""))
    config = reader.get_config()
    assert bool(config) == False
