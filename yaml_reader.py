from dataclasses import dataclass
from pathlib import Path
from typing import Any

import yaml


@dataclass
class YamlConfigReader:
    """Convert path to yaml file into dictionary with configuration parameters"""

    yaml_config_path: str | Path

    def __post_init__(self) -> None:
        """Input parameters validation"""
        if not isinstance(self.yaml_config_path, Path):
            self.yaml_config_path = Path(self.yaml_config_path)

    def get_config(self) -> dict[Any, Any]:
        """Reading config_comparison.yaml"""

        config: dict[Any, Any] = {}

        try:
            with open(self.yaml_config_path, "r") as f:
                config = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)
        except Exception as e:
            print(
                f"""Can\'t read yaml file due to error: {e}. Returns empty dictionary"""
            )
        finally:
            return config


def main():
    config_path = r"_sample_data\config_example.yaml"
    config = YamlConfigReader(config_path).get_config()
    print(config)


if __name__ == "__main__":
    main()
